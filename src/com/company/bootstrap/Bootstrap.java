package com.company.bootstrap;

import com.company.methods.BubbleSortThread;
import com.company.methods.InsertionSortThread;
import com.company.methods.SelectionSortThread;
import com.company.methods.SortThread;

public class Bootstrap {

    public void run() {
        String[] arrayStrWithoutSort = {
                "abduce",
                "fail",
                "bhutan",
                "toasty",
                "azote",
                "Juicy"
        };
        SortThread bubbleSortThread = new BubbleSortThread(arrayStrWithoutSort);
        SortThread insertSortThread = new InsertionSortThread(arrayStrWithoutSort);
        SortThread selectionSortThread = new SelectionSortThread(arrayStrWithoutSort);
        startSortThread(bubbleSortThread);
        startSortThread(insertSortThread);
        startSortThread(selectionSortThread);
    }

    private void startSortThread(SortThread sortThread) {
        Thread thread = new Thread(sortThread);
        thread.start();
    }

}
