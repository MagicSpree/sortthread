package com.company.methods;

import com.company.util.Util;

public class InsertionSortThread extends SortThread {

    public InsertionSortThread(String[] arrayString) {
        super(arrayString);
    }

    @Override
    public void sortAndPrintResult() {
        String[] strForSort = getArrayString();
        for (int i = 0; i < strForSort.length; i++) {
            int index = i;
            String temp = strForSort[i];

            while (index > 0 && compare(strForSort[index - 1], temp)) {
                strForSort[index] = strForSort[index - 1];
                index--;
            }
            strForSort[index] = temp;
        }
        Util.print("Insert sorting result: " + this);
    }

}
