package com.company.methods;

import com.company.util.Util;

public class SelectionSortThread extends SortThread {

    public SelectionSortThread(String[] arrayString) {
        super(arrayString);
    }

    @Override
    public void sortAndPrintResult() {
        String[] strForSort = getArrayString();
        for (int i = 0; i < strForSort.length; i++) {
            String minValue = strForSort[i];
            int minIndex = i;

            for (int j = i + 1; j < strForSort.length; j++) {
                if (compare(strForSort[j], minValue)) {
                    minValue = strForSort[j];
                    minIndex = j;
                }
            }
            if (i != minIndex) {
                swap(strForSort, i, minIndex);
            }
        }
        Util.print("Selection sorting result: " + this);
    }

    private void swap(String[] arrayNumber, int indexFirstNumber, int indexSecondNumber) {
        String temp = arrayNumber[indexFirstNumber];
        arrayNumber[indexFirstNumber] = arrayNumber[indexSecondNumber];
        arrayNumber[indexSecondNumber] = temp;
    }

}
