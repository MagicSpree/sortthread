package com.company.methods;

import java.util.Arrays;
import java.util.Locale;

public abstract class SortThread implements Runnable {

    private final String[] arrayString;

    protected SortThread(String[] arrayString) {
        this.arrayString = arrayString;
    }

    public void run() {
        sortAndPrintResult();
    }

    protected abstract void sortAndPrintResult();

    protected String[] getArrayString() {
        return arrayString;
    }

    protected boolean compare(String firsElement, String secondElement) {
        firsElement = firsElement.toLowerCase(Locale.ROOT);
        secondElement = secondElement.toLowerCase(Locale.ROOT);

        return firsElement.compareTo(secondElement) < 0;
    }

    public String toString() {
        return Arrays.toString(arrayString);
    }

}
