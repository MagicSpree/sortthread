package com.company.methods;

import com.company.util.Util;

public class BubbleSortThread extends SortThread {

    public BubbleSortThread(String[] arrayString) {
        super(arrayString);
    }

    @Override
    public void sortAndPrintResult() {
        String[] strForSort = getArrayString();
        for (int i = strForSort.length; i > 0; i--) {
            for (int j = 0; j < i - 1; j++) {
                if (compare(strForSort[j], strForSort[j + 1])) {
                    swap(strForSort, j);
                }
            }
        }
        Util.print("Bubble sorting result: " + this);
    }

    private void swap(String[] arrayNumber, int indexFirstNumber) {
        String temp = arrayNumber[indexFirstNumber];
        arrayNumber[indexFirstNumber] = arrayNumber[indexFirstNumber + 1];
        arrayNumber[indexFirstNumber + 1] = temp;
    }

}
